<?php
// create a 100*30 image
$im = imagecreate(30, 20);

// white background and blue text
$bg = imagecolorallocate($im, 255, 255, 255);
$textcolor = imagecolorallocate($im, 0, 0, 0);
$clear = array('red'=>255,'green'=>255,'blue'=>255);
// write the string at the top left
imagestring($im, 5, 0, 0, " ".$_GET['text']." ", $textcolor);

// output the image
header("Content-type: image/png");
imagepng($im);
?>  